! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Saddle Point
! http://users.csc.calpoly.edu/~jdalbey/103/Projects/ProgrammingPractice.html#easy - Easy 2

program saddle_point
  implicit none
  integer,dimension(:,:),allocatable :: map

  print*,'==================================='
  print*,'Programming Practice - Saddle Point'
  print*,'==================================='
  call init
  call locate

contains

  ! Create map
  subroutine init
    implicit none
    integer :: n
    
    n = 5
    allocate(map(n,n))

    open(21, file="map.txt")
    read(21,*) map
    map = transpose(map)

    return
  end subroutine init

  ! Locate saddle points, if any
  subroutine locate
    implicit none
    integer :: i
    integer :: j(1)
    logical :: check

    check = .false.
    do i = 1,5
       if (maxval(map(i,:)).eq.minval(map(:,maxloc(map(i,:))))) then
          print*,'Saddle point found at',i,j,'as -',maxval(map(i,:))
          check = .true.
       end if
    end do
    if (.not.check) print*,'No saddle points in this problem!'
    
    return
  end subroutine locate

end program saddle_point
